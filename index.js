'use strict';

const Ractive = require('ractive');

// helpers

function arrayReplaceWithNamedKeys(arr, keys) {
	let cloneArr = arr.slice(),
		buff = cloneArr.splice(cloneArr.length - keys.length)
						.reduce((acc, curr, i) => {
							acc[keys[i]] = curr;
							return acc;
						}, {});
	cloneArr.forEach((item, i) => {
		buff[i] = item;
	});
	return buff;
}

function arrayGetNamedKeys(arr) {
	return Object.keys(arr).filter(k => isNaN(parseInt(k)));
}

function arrayIndexedKeys(arr) {
	return Object.keys(arr).map(k => arr[k]);
}

function isPromise(val) {
	return !!val && (val instanceof Promise ||
		((typeof val === 'object' || typeof val === 'function') && typeof val.then === 'function'));
}

function isObject(val) {
	return !!val && val !== null && typeof val === 'object' && ! Array.isArray(val);
}

function isPojo(val) {
	return isObject(val) && Object.getPrototypeOf(val) === Object.prototype;
}

function stringify(val) {
	if (typeof val === 'string') {
		return val;
	}
	let refs = [];
	return JSON.stringify(val, (k, v) => {
		if (typeof v === 'object' && v !== null) {
			if (refs.includes(v)) return;
			refs.push(v);
		}
		return v;
	});
}

function merge(props, force) {

	const _join = (prop, obj) => {
		if (Array.isArray(this[prop])) {
			if (prop === 'adapt') {
				for (let i = 0; i < obj.length; i++) {
					let adaptor = obj[i];
					if (typeof obj[i] === 'string') {
						adaptor = this.adaptors[obj[i]];
					}
					const adapt = (this.adapt || this.viewmodel.adaptors);
					if (isObject(adaptor) && ! adapt.includes(adaptor)) {
						adapt.push(adaptor);
					}
				}
			} else if (this.hasOwnProperty(prop)) {
				this[prop] = this[prop].concat(obj);
			} else {
				for (let i = 0; i < obj.length && this[prop].push(obj[i]); i++);
			}
		} else if (isPojo(this[prop])) {
			if (prop === 'computed') {
				for (let name in obj) {
					if (typeof this.compute === 'function') {
						this.compute(name, obj[name]);
					} else {
						this.viewmodel.compute(name, obj[name]);
					}
				}
			} else {
				Object.assign(this[prop], obj);
			}
		} else if (force) {
			this[prop] = obj;
		}
	};

	for (let p in props) {
		if (typeof props[p] === 'undefined') {
			continue;
		}
		if (typeof this[p] !== 'undefined') {
			if (typeof this[p] !== typeof props[p]) {
				throw new Error("Property already exist in the instance and new value hasn't corresponding type.");
			}
			if (typeof this[p] === 'object' && props[p] !== null) {
				_join(p, props[p]);
			} else if (force) {
				this[p] = props[p];
			} else {
				throw new Error("Property already exist and can be re-assigned only in force mode.");
			}
		} else {
			this[p] = props[p];
		}
	}
	return this;
}

const asyncSetter = function(method) {

	if (typeof method !== 'function') {
		throw new TypeError("Ractive's setter undefined. Please, make sure that you use asyncSetter() properly.");
	}
	return function (keypath, value) {

		let i = 1; // presumable index of value
		if (typeof value === 'undefined') { // seems keypath unspecified
			value = keypath;
			i = 0;
		}

		return ((isPromise(value)) ? value.then((val) => {
			value = arguments[i] = val;
			return method.apply(this, arguments);
		}) : method.apply(this, arguments)).then(() => value);
	};
}.bind(Ractive.prototype);

// monkey-patching of the regular Ractive's setters to support Promises
Ractive.prototype.set = asyncSetter(Ractive.prototype.set);
Ractive.prototype.animate = asyncSetter(Ractive.prototype.animate);
Ractive.prototype.push = asyncSetter(Ractive.prototype.push);
Ractive.prototype.unshift = asyncSetter(Ractive.prototype.unshift);
Ractive.prototype.add = asyncSetter(Ractive.prototype.add);
Ractive.prototype.subtract = asyncSetter(Ractive.prototype.subtract);
Ractive.prototype.reset = asyncSetter(Ractive.prototype.reset);

Ractive.prototype.onconstruct = function () {
	this._promises = [];
	this._plugins = [];
};

Ractive.prototype.ondestruct = function () {
	delete this._promises;
	delete this._plugins;
};

/*
*  usage:
*  Ractive.assign(name, value) - assign single value to caller
*  Ractive.assign(map) - assign multiple values to caller
*  Ractive.assign(map, map, [, map....]) - eg Object.assign()
*
* */

Ractive.prototype.assign = function (options, ...args) {

	let force = false,
		len = args.length;

	if (len > 0 && typeof args[len - 1] === 'boolean' 
		&& (typeof options !== 'string' || len > 1)) {
		force = args.pop();
	}

	if ( ! isPojo(options) ) {
		if (typeof options !== 'string') {
			throw new Error('Invalid parameters in Ractive.assign');
		}
		options = {
			[options]: args[0]
		};
	} else if (args.length > 0) {
		let _merge = merge.bind(options);
		args.forEach(arg => _merge(arg, force));
		return options;
	}

	merge.call(this, options, force);

	return options;
};

Ractive.prototype.use = function (plugin, ...args) {
	const plugins = (this._plugins || (this._plugins = []));
	if (typeof plugin === 'function' && ! plugins.includes(plugin)) {
		plugin.apply(this, args);
		plugins.push(plugin);
	}
};

// adding one more promise to the array
Ractive.prototype.wait = function (promise, key) {
	const promises = (this._promises || (this._promises = []));
	return (isPromise(promise)) && ((key && (promises[key] = promise)) || promises.push(promise));
};

// collecting all async operations of current component and sub-components and resolve them when all is ready
Ractive.prototype.ready = function (callback) {

	const childComponents = this.findAllComponents().filter(c => c.isolated),
		childComponentsNames = childComponents.map(c => c.component.name),
		childPromises = childComponents.map(c => c.ready()),
		selfPromisesNames = arrayGetNamedKeys(this._promises),
		selfPromises = arrayIndexedKeys(this._promises),
		allPromises = selfPromises.concat(childPromises);

	delete this._promises;
	this._promises = [];

	return Promise.all(allPromises.map(p => p.catch(e => e)))
					.then((res) => {
						const allNames = selfPromisesNames.concat(childComponentsNames),
							data = JSON.parse(stringify(arrayReplaceWithNamedKeys(res, allNames)));

						if (typeof callback === 'function') {
							callback.call(this, null, data);
						}
						if (this === this.root) {
							this.fire('ready', {}, null, data);
						}

						return data;
					})
					.catch((err) => {
						if (typeof callback === 'function') {
							callback.call(this, err, null);
						}
						if (this === this.root) {
							this.fire('ready', {}, err, null);
						}

						throw err;
					});
};

Ractive.assign = Ractive.prototype.assign.bind(Ractive);
Ractive.use = Ractive.prototype.use.bind(Ractive);
Ractive.adaptors.promise = {
	filter: obj => isPromise(obj),
	wrap: ($$, promise, keypath) => {
		let removed = false, data = null;
		const setter = (result) => {
			if (removed) return;
			data = result;
			$$.set(keypath, result);
		};
		promise = promise.then(setter, setter);
		return {
			teardown: () => {
				if (typeof promise.cancel === 'function') {
					promise.cancel();
				}
				removed = true;
			},
			get: () => data,
			reset: () => false
		};
	}
};

module.exports = (globals) => {

	globals = globals || {};

	// optimal global default options
	const defaults = {
		enhance: true,
		append: false,
		twoway: true,
		lazy: true,
		isolated: true,
		delegate: true,
		sanitize: true,
		csp: false,
		resolveInstanceMembers: true,
		preserveWhitespace: true,
		stripComments: true,
		transitionsEnabled: true,
		nestedTransitions: true,
		noIntro: false,
		noOutro: false,
		warnAboutAmbiguity: false,
		noCssTransform: false,
		syncComputedChildren: false
	};

	globals.defaults = Object.assign(defaults, globals.defaults);

	Ractive.assign(globals, true);

	return Ractive.extend({
		adapt: ['promise']
	});
};