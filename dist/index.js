'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Ractive = require('ractive');

// helpers

function arrayReplaceWithNamedKeys(arr, keys) {
	var cloneArr = arr.slice(),
	    buff = cloneArr.splice(cloneArr.length - keys.length).reduce(function (acc, curr, i) {
		acc[keys[i]] = curr;
		return acc;
	}, {});
	cloneArr.forEach(function (item, i) {
		buff[i] = item;
	});
	return buff;
}

function arrayGetNamedKeys(arr) {
	return Object.keys(arr).filter(function (k) {
		return isNaN(parseInt(k));
	});
}

function arrayIndexedKeys(arr) {
	return Object.keys(arr).map(function (k) {
		return arr[k];
	});
}

function isPromise(val) {
	return !!val && (val instanceof Promise || ((typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object' || typeof val === 'function') && typeof val.then === 'function');
}

function isObject(val) {
	return !!val && val !== null && (typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object' && !Array.isArray(val);
}

function isPojo(val) {
	return isObject(val) && Object.getPrototypeOf(val) === Object.prototype;
}

function stringify(val) {
	if (typeof val === 'string') {
		return val;
	}
	var refs = [];
	return JSON.stringify(val, function (k, v) {
		if ((typeof v === 'undefined' ? 'undefined' : _typeof(v)) === 'object' && v !== null) {
			if (refs.includes(v)) return;
			refs.push(v);
		}
		return v;
	});
}

function merge(props, force) {
	var _this = this;

	var _join = function _join(prop, obj) {
		if (Array.isArray(_this[prop])) {
			if (prop === 'adapt') {
				for (var i = 0; i < obj.length; i++) {
					var adaptor = obj[i];
					if (typeof obj[i] === 'string') {
						adaptor = _this.adaptors[obj[i]];
					}
					var adapt = _this.adapt || _this.viewmodel.adaptors;
					if (isObject(adaptor) && !adapt.includes(adaptor)) {
						adapt.push(adaptor);
					}
				}
			} else if (_this.hasOwnProperty(prop)) {
				_this[prop] = _this[prop].concat(obj);
			} else {
				for (var _i = 0; _i < obj.length && _this[prop].push(obj[_i]); _i++) {}
			}
		} else if (isPojo(_this[prop])) {
			if (prop === 'computed') {
				for (var name in obj) {
					if (typeof _this.compute === 'function') {
						_this.compute(name, obj[name]);
					} else {
						_this.viewmodel.compute(name, obj[name]);
					}
				}
			} else {
				Object.assign(_this[prop], obj);
			}
		} else if (force) {
			_this[prop] = obj;
		}
	};

	for (var p in props) {
		if (typeof props[p] === 'undefined') {
			continue;
		}
		if (typeof this[p] !== 'undefined') {
			if (_typeof(this[p]) !== _typeof(props[p])) {
				throw new Error("Property already exist in the instance and new value hasn't corresponding type.");
			}
			if (_typeof(this[p]) === 'object' && props[p] !== null) {
				_join(p, props[p]);
			} else if (force) {
				this[p] = props[p];
			} else {
				throw new Error("Property already exist and can be re-assigned only in force mode.");
			}
		} else {
			this[p] = props[p];
		}
	}
	return this;
}

var asyncSetter = function (method) {

	if (typeof method !== 'function') {
		throw new TypeError("Ractive's setter undefined. Please, make sure that you use asyncSetter() properly.");
	}
	return function (keypath, value) {
		var _arguments = arguments,
		    _this2 = this;

		var i = 1; // presumable index of value
		if (typeof value === 'undefined') {
			// seems keypath unspecified
			value = keypath;
			i = 0;
		}

		return (isPromise(value) ? value.then(function (val) {
			value = _arguments[i] = val;
			return method.apply(_this2, _arguments);
		}) : method.apply(this, arguments)).then(function () {
			return value;
		});
	};
}.bind(Ractive.prototype);

// monkey-patching of the regular Ractive's setters to support Promises
Ractive.prototype.set = asyncSetter(Ractive.prototype.set);
Ractive.prototype.animate = asyncSetter(Ractive.prototype.animate);
Ractive.prototype.push = asyncSetter(Ractive.prototype.push);
Ractive.prototype.unshift = asyncSetter(Ractive.prototype.unshift);
Ractive.prototype.add = asyncSetter(Ractive.prototype.add);
Ractive.prototype.subtract = asyncSetter(Ractive.prototype.subtract);
Ractive.prototype.reset = asyncSetter(Ractive.prototype.reset);

Ractive.prototype.onconstruct = function () {
	this._promises = [];
	this._plugins = [];
};

Ractive.prototype.ondestruct = function () {
	delete this._promises;
	delete this._plugins;
};

/*
*  usage:
*  Ractive.assign(name, value) - assign single value to caller
*  Ractive.assign(map) - assign multiple values to caller
*  Ractive.assign(map, map, [, map....]) - eg Object.assign()
*
* */

Ractive.prototype.assign = function (options) {
	for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
		args[_key - 1] = arguments[_key];
	}

	var force = false,
	    len = args.length;

	if (len > 0 && typeof args[len - 1] === 'boolean' && (typeof options !== 'string' || len > 1)) {
		force = args.pop();
	}

	if (!isPojo(options)) {
		if (typeof options !== 'string') {
			throw new Error('Invalid parameters in Ractive.assign');
		}
		options = _defineProperty({}, options, args[0]);
	} else if (args.length > 0) {
		var _merge = merge.bind(options);
		args.forEach(function (arg) {
			return _merge(arg, force);
		});
		return options;
	}

	merge.call(this, options, force);

	return options;
};

Ractive.prototype.use = function (plugin) {
	var plugins = this._plugins || (this._plugins = []);
	if (typeof plugin === 'function' && !plugins.includes(plugin)) {
		for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
			args[_key2 - 1] = arguments[_key2];
		}

		plugin.apply(this, args);
		plugins.push(plugin);
	}
};

// adding one more promise to the array
Ractive.prototype.wait = function (promise, key) {
	var promises = this._promises || (this._promises = []);
	return isPromise(promise) && (key && (promises[key] = promise) || promises.push(promise));
};

// collecting all async operations of current component and sub-components and resolve them when all is ready
Ractive.prototype.ready = function (callback) {
	var _this3 = this;

	var childComponents = this.findAllComponents().filter(function (c) {
		return c.isolated;
	}),
	    childComponentsNames = childComponents.map(function (c) {
		return c.component.name;
	}),
	    childPromises = childComponents.map(function (c) {
		return c.ready();
	}),
	    selfPromisesNames = arrayGetNamedKeys(this._promises),
	    selfPromises = arrayIndexedKeys(this._promises),
	    allPromises = selfPromises.concat(childPromises);

	delete this._promises;
	this._promises = [];

	return Promise.all(allPromises.map(function (p) {
		return p.catch(function (e) {
			return e;
		});
	})).then(function (res) {
		var allNames = selfPromisesNames.concat(childComponentsNames),
		    data = JSON.parse(stringify(arrayReplaceWithNamedKeys(res, allNames)));

		if (typeof callback === 'function') {
			callback.call(_this3, null, data);
		}
		if (_this3 === _this3.root) {
			_this3.fire('ready', {}, null, data);
		}

		return data;
	}).catch(function (err) {
		if (typeof callback === 'function') {
			callback.call(_this3, err, null);
		}
		if (_this3 === _this3.root) {
			_this3.fire('ready', {}, err, null);
		}

		throw err;
	});
};

Ractive.assign = Ractive.prototype.assign.bind(Ractive);
Ractive.use = Ractive.prototype.use.bind(Ractive);
Ractive.adaptors.promise = {
	filter: function filter(obj) {
		return isPromise(obj);
	},
	wrap: function wrap($$, promise, keypath) {
		var removed = false,
		    data = null;
		var setter = function setter(result) {
			if (removed) return;
			data = result;
			$$.set(keypath, result);
		};
		promise = promise.then(setter, setter);
		return {
			teardown: function teardown() {
				if (typeof promise.cancel === 'function') {
					promise.cancel();
				}
				removed = true;
			},
			get: function get() {
				return data;
			},
			reset: function reset() {
				return false;
			}
		};
	}
};

module.exports = function (globals) {

	globals = globals || {};

	// optimal global default options
	var defaults = {
		enhance: true,
		append: false,
		twoway: true,
		lazy: true,
		isolated: true,
		delegate: true,
		sanitize: true,
		csp: false,
		resolveInstanceMembers: true,
		preserveWhitespace: true,
		stripComments: true,
		transitionsEnabled: true,
		nestedTransitions: true,
		noIntro: false,
		noOutro: false,
		warnAboutAmbiguity: false,
		noCssTransform: false,
		syncComputedChildren: false
	};

	globals.defaults = Object.assign(defaults, globals.defaults);

	Ractive.assign(globals, true);

	return Ractive.extend({
		adapt: ['promise']
	});
};